const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Gamelaps = db.gamelaps;
const Games = db.games;
const Users = db.users;
const Stats = db.stats;
const Userjokers = db.userjokers;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

// C => Sinek
// D => Karo
// S => Maça
// H => Kupa

exports.get = (req, res) => {

	Gamelaps.findAll({
		where: req.filters,
		order: [['createdAt', 'DESC']],
		attributes: ['id', 'game_id', 'card_1', 'card_2', 'used_joker', 'result', 'createdAt', 'updatedAt']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}

exports.updateCard = (req, res) => {

    if(req.body.card == "card_1") {
    	Gamelaps.update(
		  { card_1: createCard(0, data.lap, 3),
		  	used_joker: 1
		  },
		  { where: { id: req.body.id } }
		).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
    } else if(req.body.card == "card_2") {
    	Gamelaps.findOne({
			where: { id: req.body.id },
			attributes: ['card_1']
		}).then(data => {
			var rand = createCard(0, data.lap, 3);
			while (rand == data.dataValues.card_1) {
			  var rand = createCard(0, data.lap, 3);
			}
			Gamelaps.update(
			  { card_2: rand,
			  	used_joker: 1
			  },
			  { where: { id: req.body.id } }
			).then(result =>
			    res.status(200).json({
					"status": 200
				})
			).catch(err =>
			    res.status(500).json({
					"error": err
				})
			)
		}).catch(err => {
			res.status(500).json({
				"error": err
			});
		})

    	
    } else {
    	res.status(500).json({
			"error": "Invalid card choice"
		})
    }
	
}

exports.updateResult = (req, res) => {

    if(req.body.result == 1) {
    	Gamelaps.update(
		  { result: 1 },
		  { where: { id: req.body.id } }
		).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
    } else if(req.body.result == 2) {
    	Gamelaps.update(
		  { result: 2 },
		  { where: { id: req.body.id } }
		).then(result =>
		    res.status(200).json({
				"status": 200
			})
		).catch(err =>
		    res.status(500).json({
				"error": err
			})
		)
    } else {
    	res.status(500).json({
			"error": "Invalid result"
		})
    }
	
}

/*

1   2   3   4   5   6   7   8   9   10   11   12   13   14   15
1   2   4   8   16  32  64  128 246 512  1024 2048 4096 8192 16384

     10  15  20  25  30  35  40  45  49
1 => %15 %20 %25 %30 %35 %40 %45 %50 %55
2 => %20 %25 %30 %35 %40 %45 %50 %55 %60
3 => %25 %25 %35 %40 %45 %50 %55 %60 %65
4 => %30 %35 %40 %45 %50 %55 %60 %65 %70
5 => %35 %40 %45 %50 %55 %60 %65 %70 %75
6 => %55 %60 %65 %70 %75 %80 %85 %90 %95
7 => %62 %67 %72 %77 %82 %87 %92 %97 ---
8 => %70 %75 %80 %85 %90 %95 -----------
9 => %77 %82 %87 %92 %97 ---------------
10=> %85 %90 %95 -----------------------
11=> %92 %97 ---------------------------
12=> %100-------------------------------




*/

function createCard(balance = 0, lap = 1, choice = null, card_1 = null) {
	//return Math.floor(Math.random() * 13) + 2 + ""+ Math.ceil(Math.random() * 4) +"";
	if(choice == 3) {
		return Math.floor(Math.random() * 11) + 2 + ""+ Math.ceil(Math.random() * 4) +"";
	} else {
		if(lap <= 5) {
			var factor = 10;
		} else {
			var factor = 15;
		}

		var totalChance = ((balance * 2) + (lap * factor)) / 2;

		var random = Math.round(Math.random() * 100);

		card_1 = parseInt(card_1.slice(0, -1));

		var rand = Math.random();

		if(choice == 1) {
			if(totalChance > random) {
				// Lost
				var createdCard = Math.ceil(rand * (14 - card_1)) + card_1 + ""+ Math.ceil(rand * 4) +"";
				console.log("card_1", card_1);
				console.log("createdCard", createdCard);
			} else {
				// Won
				var createdCard = Math.floor(rand * (card_1 - 2)) + 2 + ""+ Math.ceil(rand * 4) +"";
				console.log("card_1", card_1);
				console.log("createdCard", createdCard);
			}
		} else if(choice == 2) {
			if(totalChance > random) {
				// Lost
				var createdCard = Math.floor(rand * (card_1 - 2)) + 2 + ""+ Math.ceil(rand * 4) +"";
				console.log("card_1", card_1);
				console.log("createdCard", createdCard);
			} else {
				// Won
				var createdCard = Math.ceil(rand * (14 - card_1)) + card_1 + ""+ Math.ceil(rand * 4) +"";
				console.log("card_1", card_1);
				console.log("createdCard", createdCard);
			}
		}

		return createdCard;
	}
} 

exports.lessThan = (req, res) => {

	// Oyun kontrol ediliyor
	Games.findOne({
		where: { user_id: req.userId, is_completed: 0 },
		attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt']
	}).then(data => {
		var game = data.dataValues;

		// Tur kontrol ediliyor
		Gamelaps.findOne({
			where: { game_id: game.id, result: 0 },
			attributes: ['id', 'game_id', 'card_1', 'card_2', 'used_joker', 'result', 'createdAt', 'updatedAt']
		}).then(data => {
			var lap = data.dataValues;
			if(lap.card_2 == null) {
				var lapCard2 = createCard(0, game.lap, 1, lap.card_1);
			} else {
				var lapCard2 = lap.card_2;
			}
			if(parseInt(lap.card_1) > parseInt(lapCard2)) {
				var lapResult = 1;
			} else {
				var lapResult = 2;
			}

			// Tur güncelleniyor
			Gamelaps.update(
				{ card_2: lapCard2,
				result: lapResult },
				{ where: { id: lap.id } }
			).then(result => {
				lap.card_2 = lapCard2;
				lap.result = lapResult;
				if(lapResult == 1) {
					var isCompleted = 0;
					if(game.lap == 1) {
						var coins = 1;
					} else {
						var coins = game.coins * 2;
					}
				} else {
					var isCompleted = 1;
					var coins = 0;
				}
				
				// Oyun güncelleniyor
				Games.update(
					{ is_completed: isCompleted,
					coins: coins },
					{ where: { id: game.id } }
				).then(result => {
					game.coins = coins;
					game.is_completed = isCompleted;
					res.status(200).json({
						"game": game,
						"lap": lap
					});
				}).catch(err => {
					res.status(200).json({
						"error": err,
						"description": "Kart oluşturulamadı..."
					});
				})
				
			}).catch(err => {
				res.status(200).json({
					"error": err,
					"description": "Kart oluşturulamadı..."
				});
			})
		}).catch(err => {
			res.status(200).json({
				"error": err,
				"description": "Tur bulunamadı veya tamamlanmış..."
			});
		})
	}).catch(err => {
		res.status(200).json({
			"error": err,
			"description": "Oyun bulunamadı veya bir hata oluştu..."
		});
	})
	
}

exports.greaterThan = (req, res) => {

	// Oyun kontrol ediliyor
	Games.findOne({
		where: { user_id: req.userId, is_completed: 0 },
		attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt']
	}).then(data => {
		var game = data.dataValues;

		// Tur kontrol ediliyor
		Gamelaps.findOne({
			where: { game_id: game.id, result: 0 },
			attributes: ['id', 'game_id', 'card_1', 'card_2', 'used_joker', 'result', 'createdAt', 'updatedAt']
		}).then(data => {
			var lap = data.dataValues;
			if(lap.card_2 == null) {
				var lapCard2 = createCard(0, game.lap, 2, lap.card_1);
			} else {
				var lapCard2 = lap.card_2;
			}
			if(parseInt(lap.card_1) < parseInt(lapCard2)) {
				var lapResult = 1;
			} else {
				var lapResult = 2;
			}

			// Tur güncelleniyor
			Gamelaps.update(
				{ card_2: lapCard2,
				result: lapResult },
				{ where: { id: lap.id } }
			).then(result => {
				lap.card_2 = lapCard2;
				lap.result = lapResult;
				if(lapResult == 1) {
					var isCompleted = 0;
					if(game.lap == 1) {
						var coins = 1;
					} else {
						var coins = game.coins * 2;
					}
				} else {
					var isCompleted = 1;
					var coins = 0;
				}
				
				// Oyun güncelleniyor
				Games.update(
					{ is_completed: isCompleted,
					coins: coins },
					{ where: { id: game.id } }
				).then(result => {
					game.coins = coins;
					game.is_completed = isCompleted;
					res.status(200).json({
						"game": game,
						"lap": lap
					});
				}).catch(err => {
					res.status(200).json({
						"error": err,
						"description": "Kart oluşturulamadı..."
					});
				})
				
			}).catch(err => {
				res.status(200).json({
					"error": err,
					"description": "Kart oluşturulamadı..."
				});
			})
		}).catch(err => {
			res.status(200).json({
				"error": err,
				"description": "Tur bulunamadı veya tamamlanmış..."
			});
		})
	}).catch(err => {
		res.status(200).json({
			"error": err,
			"description": "Oyun bulunamadı veya bir hata oluştu..."
		});
	})
	
}

exports.getPrize = (req, res) => {
	// Oyun kontrol ediliyor
	Games.findOne({
		where: { id: req.body.id, user_id: req.userId, is_completed: 0 },
		attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt']
	}).then(data => {
		var game = data.dataValues;

		Games.update(
			{ is_completed: 1 },
			{ where: { id: game.id } }
		).then(result => {
			game.is_completed = 1;
			Users.findOne({
				where: { id: req.userId },
				attributes: ['id', 'balance']
			}).then(data => {
				var user = data.dataValues;
				Users.update(
					{ balance: parseInt(user.balance) + game.coins },
					{ where: { id: user.id } }
				).then(result => {
					Stats.increment(
						{ total_coins: game.coins }, 
						{ where: { id: 1 } 
					}).then(result =>
					    res.status(200).json({
							"game": game
						})
					).catch(err =>
					    res.status(500).json({
							"error": err
						})
					)
				}).catch(err => {
					res.status(200).json({
						"error": err,
						"description": "Oyun bitirilemedi..."
					});
				})
			}).catch(err => {
				res.status(200).json({
					"error": err,
					"description": "Oyun bulunamadı veya bir hata oluştu..."
				});
			})
			
		}).catch(err => {
			res.status(200).json({
				"error": err,
				"description": "Oyun bitirilemedi..."
			});
		})
	}).catch(err => {
		res.status(200).json({
			"error": err,
			"description": "Oyun bulunamadı veya bir hata oluştu..."
		});
	})
	
}

exports.continueGame = (req, res) => {
	// Oyun kontrol ediliyor
	Games.findOne({
		where: { id: req.body.id, user_id: req.userId, is_completed: 0 },
		attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt']
	}).then(data => {
		var game = data.dataValues;

		Games.update(
			{ lap: parseInt(game.lap) + 1 },
			{ where: { id: game.id } }
		).then(result => {
			game.lap = parseInt(game.lap) + 1;
			Gamelaps.create({
				game_id: game.id,
				card_1: createCard(0, data.lap, 3),
				result: 0
			}).then(lap => {
				lap.used_joker = 0;
				Stats.increment(
					{ total_laps: 1 }, 
					{ where: { id: 1 } 
				}).then(result =>
				    res.status(200).json({
						"game": game,
						"lap": lap
					})
				).catch(err =>
				    res.status(500).json({
						"error": err
					})
				)
				
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			})
		}).catch(err => {
			res.status(200).json({
				"error": err,
				"description": "Oyun bitirilemedi..."
			});
		})
	}).catch(err => {
		res.status(200).json({
			"error": err,
			"description": "Oyun bulunamadı veya bir hata oluştu..."
		});
	});
	
}

exports.useJoker = (req, res) => {

	    if(req.body.type == "pass") {

	    	// 1. Adım kullanıcının jokerleri kontrol ediliyor
			Userjokers.findOne({
				where: { user_id: req.userId },
				attributes: ['pass']
			}).then(data => {
				if(data.dataValues.pass == 0) {
					res.status(200).json({
			    		"status": 500,
						"error": "You dont have a joker."
					})
				} else {
					// 2. Adım kullanıcının bu oyununda joker kullanıp kullanmadığı kontrol ediliyor
					Gamelaps.findOne({
						where: { game_id: req.body.game_id },
						attributes: [[db.Sequelize.fn('sum', db.Sequelize.col('used_joker')), 'is_used']],
						group: ['game_id']
					}).then(data => {
						if(data.dataValues.is_used == 1) {
							res.status(200).json({
					    		"status": 500,
								"error": "You have already used joker in this lap."
							})
						} else {
							// 3. Adım oyun bilgisi çekiliyor
							Games.findOne({
								where: { user_id: req.userId, is_completed: 0 },
								attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt']
							}).then(data => {
								// 4. Adım joker kullanılarak yeni kart üretiliyor
								Gamelaps.update(
								  { card_1: createCard(0, data.dataValues.lap, 3),
								  	used_joker: 1
								  },
								  { where: { id: req.body.lap_id } }
								).then(result => {
									// 5. Adım kullanıcının jokerinden 1 adet düşülüyor
								    Userjokers.decrement(
										{ pass: 1 }, 
										{ where: { user_id: req.userId } 
									}).then(result => {
										// 6. Adım tur bilgileri yenileniyor
									    Gamelaps.findOne({
											where: { result: 0, game_id: req.body.game_id },
											attributes: ['id', 'game_id', 'card_1', 'card_2', 'used_joker', 'result', 'createdAt', 'updatedAt']
										}).then(data => {
											res.status(200).json({
									    		"status": 200,
												"data": data.dataValues
											})
										}).catch(err => {
											res.status(200).json({
									    		"status": 500,
												"error": "You have already used joker in this lap."
											})
										})
									}).catch(err =>
									    res.status(200).json({
								    		"status": 500,
											"error": "You have already used joker in this lap."
										})
									)
								}).catch(err =>
								    res.status(200).json({
							    		"status": 500,
										"error": "You have already used joker in this lap."
									})
								)
							}).catch(err => {
								res.status(200).json({
							    	"status": 500,
									"error": "Sorry, an error has occurred."
								})
							})
							
							
						}
					}).catch(err => {
						res.status(200).json({
					    	"status": 500,
							"error": "Sorry, an error has occurred."
						})
					})
				}
			}).catch(err => {
				res.status(200).json({
			    	"status": 500,
					"error": "Sorry, an error has occurred."
				})
			})
	    } else if(req.body.type == "show_result") {
	    	// 1. Adım kullanıcının jokerleri kontrol ediliyor
			Userjokers.findOne({
				where: { user_id: req.userId },
				attributes: ['show_result']
			}).then(data => {
				if(data.dataValues.show_result == 0) {
					res.status(200).json({
			    		"status": 500,
						"error": "You dont have a joker."
					})
				} else {
					// 2. Adım kullanıcının bu oyununda joker kullanıp kullanmadığı kontrol ediliyor
					Gamelaps.findOne({
						where: { game_id: req.body.game_id },
						attributes: [[db.Sequelize.fn('sum', db.Sequelize.col('used_joker')), 'is_used']],
						group: ['game_id']
					}).then(data => {
						if(data.dataValues.is_used == 1) {
							res.status(200).json({
					    		"status": 500,
								"error": "You have already used joker in this lap."
							})
						} else {
							// 3. Adım oyun bilgisi çekiliyor
							Games.findOne({
								where: { user_id: req.userId, is_completed: 0 },
								attributes: ['id', 'user_id', 'lap', 'coins', 'is_completed', 'createdAt']
							}).then(data => {
								// 4. Adım joker kullanılarak yeni kart üretiliyor
								Gamelaps.update(
								  { card_2: createCard(0, data.dataValues.lap, 3),
								  	used_joker: 1
								  },
								  { where: { id: req.body.lap_id } }
								).then(result => {
									// 5. Adım kullanıcının jokerinden 1 adet düşülüyor
								    Userjokers.decrement(
										{ show_result: 1 }, 
										{ where: { user_id: req.userId } 
									}).then(result => {
										// 6. Adım tur bilgileri yenileniyor
									    Gamelaps.findOne({
											where: { result: 0, game_id: req.body.game_id },
											attributes: ['id', 'game_id', 'card_1', 'card_2', 'used_joker', 'result', 'createdAt', 'updatedAt']
										}).then(data => {
											res.status(200).json({
									    		"status": 200,
												"data": data.dataValues
											})
										}).catch(err => {
											res.status(200).json({
									    		"status": 500,
												"error": "You have already used joker in this lap."
											})
										})
									}).catch(err =>
									    res.status(200).json({
								    		"status": 500,
											"error": "You have already used joker in this lap."
										})
									)
								}).catch(err =>
								    res.status(200).json({
							    		"status": 500,
										"error": "You have already used joker in this lap."
									})
								)
							}).catch(err => {
								res.status(200).json({
							    	"status": 500,
									"error": "Sorry, an error has occurred."
								})
							})
							
							
						}
					}).catch(err => {
						res.status(200).json({
					    	"status": 500,
							"error": "Sorry, an error has occurred."
						})
					})
				}
			}).catch(err => {
				res.status(200).json({
			    	"status": 500,
					"error": "Sorry, an error has occurred."
				})
			})
	    } else {
	    	res.status(200).json({
	    		"status": 500,
				"error": "Invalid joker"
			})
	    }
		
	}