const db = require('../config/db.config.js');
const Blacklist = db.blacklist;

exports.get = (req, res) => {
	Blacklist.findAll({}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}