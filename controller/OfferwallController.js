const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Offerwall = db.offerwall;
const Users = db.users;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	var where = {user_id: req.userId};
	if(req.query.ref && req.query.ref == 1) {
		where = {user_id: req.userId, ref_id: {
			[Op.ne]: null
		}};
	}
	
	Offerwall.findAll({
		where: where,
		attributes: ['user_id', 'tx_id', 'point_value', 'usd_value', 'offer_title', 'is_completed', 'media', 'complete_date', 'offer_id', 'ref_id'],
		include:[{
			model: Users, as: 'users',
			attributes: ['username'],
            required: false
         }]
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}