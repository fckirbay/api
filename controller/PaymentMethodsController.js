const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Paymentmethods = db.paymentmethods;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {
	Paymentmethods.findAll({
		where: req.filters,
		order: [['orders', 'ASC']],
		attributes: ['id', 'name', 'subtitle', 'photo', 'url', 'coin', 'status']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})
}