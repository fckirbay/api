const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Scratches = db.scratches;
const Coins = db.coins;
const Usercoins = db.usercoins;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	req.filters['user_id'] = req.userId;

	Scratches.findAll({
		where: req.filters,
		attributes: ['user_id', 'box_1', 'box_2', 'box_3', 'box_4', 'box_5', 'box_6', 'box_7', 'box_8', 'box_9', 'win', 'status', 'amount', 'created_at']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.post = (req, res) => {

	Scratches.findOne({
        where: { id: req.body.scratchId, user_id: req.userId }
    }).then(scratch => {
		if(scratch.status == 0) {
            if(scratch.win == null) {
				Scratches.update(
					{ status: 2 }, 
					{ where: { id: req.body.scratchId, user_id: req.userId } 
				}).then(result => {
					res.status(200).json({
						"status": 200,
						"message": 'NO_WINNINGS'
					});
				}).catch(err =>
					res.status(500).json({
						"error": err
					})
				);
            } else {
                Coins.findOne({
                    where: { symbol: scratch.win.toUpperCase() } 
                }).then(coin => {
                    Usercoins.findOne({
                        where: { user_id: req.userId, coin_id: coin.id } 
                    }).then(usercoin => {
                        if(usercoin == null) {
                            Usercoins.create({
                                user_id: req.userId,
                                coin_id: coin.id,
                                total: scratch.amount
                            }).then(saveResult => {
                                Scratches.update(
                                    { status: 1 }, 
                                    { where: { id: req.body.scratchId, user_id: req.userId } 
                                }).then(result =>
                                    res.status(200).json({
                                        "status": 200,
										"message": "OK"
                                    })
                                ).catch(err =>
                                    res.status(500).json({
                                        "error": err
                                    })
                                );
                            }).catch(err => {
                                res.status(500).send("Fail! Error -> " + err);
                            });
                        } else {
							Usercoins.increment(
								{ total: scratch.amount }, 
								{ where: { user_id: req.userId, coin_id: coin.id } 
							}).then(result => {
								Scratches.update(
                                    { status: 1 }, 
                                    { where: { id: req.body.scratchId, user_id: req.userId } 
                                }).then(result =>
                                    res.status(200).json({
                                        "status": 200,
										"message": "OK"
                                    })
                                ).catch(err =>
                                    res.status(500).json({
                                        "error": err
                                    })
                                );
							}).catch(err => {
								res.status(500).json({
									"error": err
								});
							});
						}
                    }).catch(err =>
                        res.status(500).json({
                            "error": err
                        })
                    );
				}).catch(err =>
                    res.status(500).json({
                        "error": err
                    })
                );
            }
		} else {
			res.status(200).json({
				"status": 200,
				"message": 'ALREADY_COMPLETED'
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
	
};