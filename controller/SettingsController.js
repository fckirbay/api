const db = require('../config/db.config.js');
const Settings = db.settings;

exports.get = (req, res) => {
	Settings.findOne({
		attributes: ['ads', 'offerwall', 'version']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
}