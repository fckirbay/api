const db = require('../config/db.config.js');
var moment = require('moment');
const Op = db.Sequelize.Op;
const Puzzle = db.puzzle;
const Users = db.users;

exports.get = (req, res) => {

	Puzzle.findAll({
		where: {
			user_id: req.userId,
			datetime: {
                [Op.gte]: moment().utc().startOf('day').toDate()
            }
		},
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.getlist = (req, res) => {

	if(req.query.toggle == 0) {
		Puzzle.findAll({
			include:[{
				model: Users, as: 'users'
			}],
			where: {
				datetime: {
					[Op.gte]: moment().utc().startOf('day').toDate()
				},
				score: {
					[Op.gt]: 0
				}
			},
			order: [['score', 'DESC']],
			limit: 100
		}).then(data => {
			res.status(200).json({
				"data": data
			});
		}).catch(err => {
			res.status(500).json({
				"error": err
			});
		});
	} else if(req.query.toggle == 1) {
		Puzzle.findAll({
			include:[{
				model: Users, as: 'users'
			}],
			where: {
				datetime: {
					[Op.gte]: moment().subtract(1, 'd').startOf('day').toDate()
				},
				datetime: {
					[Op.lte]: moment().subtract(1, 'd').endOf('day').toDate()
				},
				score: {
					[Op.gt]: 0
				}
			},
			order: [['score', 'DESC']],
			limit: 100
		}).then(data => {
			res.status(200).json({
				"data": data
			});
		}).catch(err => {
			res.status(500).json({
				"error": err
			});
		});
	} else if(req.query.toggle == 2) {
		Puzzle.findAll({
			include:[{
				model: Users, as: 'users'
			}],
			where: {
				score: {
					[Op.gt]: 0
				}
			},
			order: [['score', 'DESC']],
			limit: 100
		}).then(data => {
			res.status(200).json({
				"data": data
			});
		}).catch(err => {
			res.status(500).json({
				"error": err
			});
		});
	}
	
};

exports.post = (req, res) => {
	Puzzle.findOne({
        where: {
			user_id: req.userId,
			datetime: {
                [Op.gte]: moment().startOf('day').toDate()
            }
		}
    }).then(puzzleGame => {
		if(puzzleGame == null) {
			Puzzle.create({
				user_id: req.userId,
				score: 0,
				datetime: moment().format("YYYY-MM-DD HH:mm:ss"),
			}).then(puzzleCreate => {
				res.status(200).json({
					"status": 200,
					"message": 'OK'
				});
			}).catch(err => {
				res.status(500).send("Fail! Error -> " + err);
			});
		} else {
			res.status(200).json({
				"status": 200,
				"message": 'ALREADY_COMPLETED'
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.update = (req, res) => {
	Puzzle.findOne({
        where: {
			user_id: req.userId,
			datetime: {
                [Op.gte]: moment().startOf('day').toDate()
            }
		}
    }).then(puzzleGame => {
		if(puzzleGame == null) {
			res.status(200).json({
				"status": 200,
				"message": 'NO_GAME_FOUND'
			});
		} else {
			Puzzle.update({
				score: req.body.score,
				completed_at: moment().format("YYYY-MM-DD HH:mm:ss")
			}, { where: { id: puzzleGame['id'] } }).then(updateResult => {
				console.log("operation completed");
			}).catch(err => {
				console.log(err, "Coinmarketcap service could not executed...");
			});
		}
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};