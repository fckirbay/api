const db = require('../config/db.config.js');
const config = require('../config/config.js');
const Coins = db.coins;

const Op = db.Sequelize.Op;

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');

exports.get = (req, res) => {

	var coinList = ['AVAX', 'BNB', 'BTC', 'BTT', 'CHZ', 'DOGE', 'DOT', 'EOS', 'ETH', 'HOT', 'LINK', 'NEO', 'ONT', 'RVN',
	'SXP', 'TRX', 'USDT', 'VET', 'XLM', 'XRP'];
	
	const rp = require('request-promise');
	const requestOptions = {
	  method: 'GET',
	  uri: 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest',
	  qs: {
	    'start': '1',
	    'limit': '5000',
	    'convert': 'USD'
	  },
	  headers: {
	    'X-CMC_PRO_API_KEY': 'be89e777-3c79-4887-8bad-8e6d71d27752'
	  },
	  json: true,
	  gzip: true
	};

	rp(requestOptions).then(response => {
		var count = 0;
		console.log("cont", count);
		response.data.forEach(element => {
			console.log("each");
			if(coinList.includes(element.symbol)) {
				Coins.update({
					cmc_rank: element.cmc_rank,
					circulating_supply: element.circulating_supply,
					total_supply: element.total_supply,
					max_supply: element.max_supply,
					price: element.quote.USD.price,
					volume_24h: element.quote.USD.volume_24h,
					percent_change_1h: element.quote.USD.percent_change_1h,
					percent_change_24h: element.quote.USD.percent_change_24h,
					percent_change_7d: element.quote.USD.percent_change_7d,
					market_cap: element.quote.USD.market_cap
				}, { where: { symbol: element.symbol } }).then(updateResult => {
					count++;
					if(count == response.data.length) {
						console.log("Coinmarketcap service executed...");
					}
				}).catch(err => {
					console.log(err, "Coinmarketcap service could not executed...");
				});
			} else {
				console.log("xxx");
				count++;
				if(count == response.data.length) {
					console.log("Coinmarketcap service executed...");
				}
			}
		});
	}).catch((err) => {
	  console.log('API call error:', err.message);
	});

}