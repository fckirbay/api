const db = require("../config/db.config.js");
const config = require("../config/config.js");
const Offerwall = db.offerwall;
const Offers = db.offers;

const User = db.user;
const Notifications = db.notifications;

const Op = db.Sequelize.Op;

var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");
var moment = require("moment");

exports.get = (req, res) => {
  /*Users.findOne({
		where: req.filters,
		attributes: ['id', 'name', 'username', 'email', 'first_name', 'last_name', 'createdAt', 'updatedAt', 'phone', 'photo', 'reference', 'ticket', 'clicks', 'won', 'lost', 'earnings', 'balance', 'reference_id', 'reference_count', 'blocked', 'blocked_reason', 'lang', 'currency', 'country', 'os', 'firebase', 'walkthrough', 'verification', 'verification_tries', 'purchasing', 'premium_membership', 'premium_expiration', 'is_notify']
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	})*/

  // NOTIFICATION GÖNDERİMİ EKLENECEK!!!

  if (req.query.media == "ayetstudios") {
    var user_id = req.query.user_id;
    var tx_id = req.query.tx_id;
    var point_value = req.query.point_value;
    var usd_value = req.query.usd_value;
    var offer_title = req.query.offer_title;
    var media = req.query.media;

    Offerwall.findOne({
      where: { user_id: user_id, tx_id: tx_id },
      attributes: [
        "user_id",
        "tx_id",
        "point_value",
        "usd_value",
        "offer_title",
        "is_completed",
        "complete_date",
      ],
    })
      .then((data) => {
        if (data == null) {
          Offerwall.create({
            user_id: user_id,
            tx_id: tx_id,
            point_value: point_value,
            usd_value: usd_value,
            offer_title: offer_title,
            is_completed: 1,
            media: media,
            complete_date: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
          })
            .then((saveResult) => {
              User.increment(
                { ticket: point_value },
                { where: { id: user_id } }
              )
                .then((result) =>
                  User.findOne({
                    where: { id: user_id },
                    attributes: ["firebase"],
                  })
                    .then((user) => {
                      Notifications.bulkCreate([
                        {
                          user_id: user_id,
                          title: "Congratulations!",
                          notification:
                            "You have earned " +
                            point_value +
                            " key from the " +
                            offer_title +
                            " you completed.",
                          firebase: user.firebase,
                          time: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
                          status: 0,
                          created_at: moment()
                            .utc()
                            .format("YYYY-MM-DD HH:mm:ss"),
                        },
                        {
                          user_id: 1,
                          title: "Hesaba para geldi!",
                          notification:
                            "$" +
                            usd_value +
                            " tutarında bir görev tamamlandı!",
                          firebase: "ceda9c59-f3de-4f05-917f-8f757326c77b",
                          time: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
                          status: 0,
                          created_at: moment()
                            .utc()
                            .format("YYYY-MM-DD HH:mm:ss"),
                        },
                        {
                          user_id: 43,
                          title: "Hesaba para geldi!",
                          notification:
                            "$" +
                            usd_value +
                            " tutarında bir görev tamamlandı!",
                          firebase: "6834f0bf-b691-4c43-ac9d-b7419fba8017",
                          time: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
                          status: 0,
                          created_at: moment()
                            .utc()
                            .format("YYYY-MM-DD HH:mm:ss"),
                        },
                      ])
                        .then(() => {
                          // Notice: There are no arguments here, as of right now you'll have to...
                          res.status(200).json({
                            status: 200,
                          });
                        })
                        .catch((err) => {
                          res.status(500).send("Fail! Error -> " + err);
                        });
                    })
                    .catch((err) => {
                      res.status(500).json({
                        description: "Can not access Management Board",
                        error: err,
                      });
                    })
                )
                .catch((err) =>
                  res.status(500).json({
                    error: err,
                  })
                );
            })
            .catch((err) => {
              res.status(500).send("Fail! Error -> " + err);
            });
        } else {
          res.status(500).json({
            error: "Duplicate!",
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  } else {
    var user_id = req.query.user_id;
    var tx_id = req.query.tx_id;
    var usd_value = req.query.usd_value;
    var media = req.query.media;
    var offer_id = req.query.offer_id;

    Offerwall.findOne({
      where: { user_id: user_id, tx_id: tx_id },
      attributes: [
        "user_id",
        "tx_id",
        "point_value",
        "usd_value",
        "offer_title",
        "is_completed",
        "complete_date",
      ],
    })
      .then((data) => {
        if (data == null) {
          Offers.findOne({
            where: { offer_id: offer_id },
            attributes: ["payout", "name"],
          })
            .then((off) => {
              Offerwall.create({
                user_id: user_id,
                tx_id: tx_id,
                point_value: off.payout,
                usd_value: usd_value,
                offer_title: off.acnhor,
                is_completed: 1,
                media: media,
                complete_date: moment().utc().format("YYYY-MM-DD HH:mm:ss"),
              })
                .then((saveResult) => {
                  User.increment(
                    { ticket: off.payout },
                    { where: { id: user_id } }
                  )
                    .then((result) =>
                      User.findOne({
                        where: { id: user_id },
                        attributes: ["firebase"],
                      })
                        .then((user) => {
                          Notifications.bulkCreate([
                            {
                              user_id: user_id,
                              title: "Congratulations!",
                              notification:
                                "You have earned " +
                                off.payout +
                                " key from the " +
                                off.name +
                                " you completed.",
                              firebase: user.firebase,
                              time: moment()
                                .utc()
                                .format("YYYY-MM-DD HH:mm:ss"),
                              status: 0,
                              created_at: moment()
                                .utc()
                                .format("YYYY-MM-DD HH:mm:ss"),
                            },
                            {
                              user_id: 1,
                              title: "Hesaba para geldi!",
                              notification:
                                "$" +
                                usd_value +
                                " tutarında bir görev tamamlandı!",
                              firebase: "ceda9c59-f3de-4f05-917f-8f757326c77b",
                              time: moment()
                                .utc()
                                .format("YYYY-MM-DD HH:mm:ss"),
                              status: 0,
                              created_at: moment()
                                .utc()
                                .format("YYYY-MM-DD HH:mm:ss"),
                            },
                          ])
                            .then(() => {
                              // Notice: There are no arguments here, as of right now you'll have to...
                              res.status(200).json({
                                status: 200,
                              });
                            })
                            .catch((err) => {
                              res.status(500).send("Fail! Error -> " + err);
                            });
                        })
                        .catch((err) => {
                          res.status(500).json({
                            description: "Can not access Management Board",
                            error: err,
                          });
                        })
                    )
                    .catch((err) =>
                      res.status(500).json({
                        error: err,
                      })
                    );
                })
                .catch((err) => {
                  res.status(500).send("Fail! Error -> " + err);
                });
            })
            .catch((err) => {
              res.status(500).json({
                description: "Can not access Management Board",
                error: err,
              });
            });
        } else {
          res.status(500).json({
            error: "Duplicate!",
          });
        }
      })
      .catch((err) => {
        res.status(500).json({
          error: err,
        });
      });
  }
};
