const db = require('../config/db.config.js');
var moment = require('moment');
const Withdraws = db.withdraws;
const UserCoins = db.usercoins;
const Coins = db.coins;

exports.get = (req, res) => {

    req.filters['user_id'] = req.userId;

	Withdraws.findAll({
        where: req.filters,
        include:[{
			model: Coins, as: 'coins', 
            required:false
         }]
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.post = (req, res) => {

    Coins.findOne({
        where: { 'id': req.body.coinId },
        attributes: ['price']
    }).then(coin => {
        UserCoins.findOne({
            where: { 'user_id': req.userId, 'coin_id': req.body.coinId },
            attributes: ['total']
        }).then(usercoin => {
            if(usercoin == null) {
                res.status(200).json({
                    "status": 200,
                    "message": "INSUFFICIENT_BALANCE"
                });
            } else if(coin.price * usercoin.total < 5) {
                res.status(200).json({
                    "status": 200,
                    "message": "INSUFFICIENT_BALANCE"
                });
            } else {
                UserCoins.update(
                    { total: 0 }, 
                    { where: { user_id: req.userId, coin_id: req.body.coinId } 
                }).then(result =>
                    Withdraws.create({
                        user_id: req.userId,
                        coin_id: req.body.coinId,
                        amount: usercoin.total,
                        status: 0,
                        created_at: moment().toDate()
                    }).then(game => {
                        res.status(200).json({
                            "status": 200,
                            "message": "OK"
                        });
                    }).catch(err => {
                        res.status(500).send("Fail! Error -> " + err);
                    })
                ).catch(err =>
                    res.status(500).json({
                        "error": err
                    })
                );
            }
        }).catch(err => {
            res.status(500).json({
                "error": err
            });
        })
    }).catch(err => {
        res.status(500).json({
            "error": err
        });
    });
    
};