const db = require('../config/db.config.js');
const Offers = db.offers;
const Op = db.Sequelize.Op;
var request = require('request');
var geoip = require('geoip-country');

exports.get = (req, res) => {
	var userAgent = 'android';
	var userIp = req.connection.remoteAddress;

	var country = 'TR';
	if(geoip.lookup(userIp)) {
		country = geoip.lookup(userIp).country;
	}
	
	var params = {
		[Op.or]: [{user_agent: ''}, {user_agent: 'android'}],
		countries: {[Op.like]: '%'+country+'%'},
		status: 1
	};
	if(req.query.user_agent == 2) {
		params = {
			[Op.or]: [{user_agent: ''}, {user_agent: 'ios'}, {user_agent: 'iphone'}, {user_agent: 'ipad'}],
			countries: {[Op.like]: '%'+country+'%'},
			status: 1
		};
	}
	Offers.findAll({
		where: params,
		order: [['id', 'ASC']],
	}).then(data => {
		res.status(200).json({
			"data": data
		});
	}).catch(err => {
		res.status(500).json({
			"error": err
		});
	});
};

exports.adgate = (req, res) => {

	var successCount = 0;
	var errorCount = 0;
	var updatedCount = 0;
	var updateArray = [];
	request({
	  url: 'https://api.adgatemedia.com/v2/offers?aff=61555&api_key=c8fc77b81b21db2cae808b995d95dd2d&wall_code=na6bpw',
	  json: true
	}, function(error, response, body) {
		if(error) {
			res.status(500).json({
				"error": error
			});
		} else {
			Offers.update({ status: 0 },
				{ where: { media: 'adgate' } })
			.then(updateResult => {
				
				body.data.forEach(element => {
					Offers.findOne({
						where: { offer_id: element.id },
						attributes: ['offer_id']
					}).then(isOffer => {
						if(isOffer == null) {
							if(element.user_agent.length > 0 && (element.user_agent[0] == 'android' || element.user_agent[0] == 'ios' || element.user_agent[0] == 'iphone')) {
								Offers.create({
									offer_id: element.id,
									name: element.name,
									anchor: element.anchor,
									requirements: element.requirements,
									user_agent: element.user_agent.toString(),
									mobile_only: element.mobile_only,
									icon: element.creatives.icon,
									epc: element.epc,
									categories: element.categories.toString(),
									cpm: element.adgate_rewards.cpm,
									description: element.adgate_rewards.description,
									countries: element.countries.toString(),
									offer_type: element.type,
									payout: element.payout,
									media: 'adgate',
									click_url: element.click_url,
									support_url: element.support_url,
									preview_url: element.preview_url,
									status: 1
								}).then(response => {
									successCount++;
									if(successCount + errorCount + updatedCount == body.data.length) {
										Offers.update({ status: 1 },
											{ where: { offer_id: updateArray } })
										.then(updateResult => {
											res.status(200).json({
												"success_count": successCount,
												"errorCount": errorCount,
												"updatedCount": updatedCount
											});	
										}).catch(err => {
											console.log(err, "Coinmarketcap service could not executed...");
										});
									}
								}).catch(err => {
									console.log("err", err);
									errorCount++;
									if(successCount + errorCount + updatedCount == body.data.length) {
										Offers.update({ status: 1 },
											{ where: { offer_id: updateArray } })
										.then(updateResult => {
											res.status(200).json({
												"success_count": successCount,
												"errorCount": errorCount,
												"updatedCount": updatedCount
											});	
										}).catch(err => {
											console.log(err, "Coinmarketcap service could not executed...");
										});
									}
								});
							} else {
								successCount++;
								if(successCount + errorCount + updatedCount == body.data.length) {
									Offers.update({ status: 1 },
										{ where: { offer_id: updateArray } })
									.then(updateResult => {
										res.status(200).json({
											"success_count": successCount,
											"errorCount": errorCount,
											"updatedCount": updatedCount
										});	
									}).catch(err => {
										console.log(err, "Coinmarketcap service could not executed...");
									});
								}
							}
							
						} else {
							updateArray.push(element.id);
							updatedCount++;
							if(successCount + errorCount + updatedCount == body.data.length) {
								Offers.update({ status: 1 },
									{ where: { offer_id: updateArray } })
								.then(updateResult => {
									res.status(200).json({
										"success_count": successCount,
										"errorCount": errorCount,
										"updatedCount": updatedCount
									});	
								}).catch(err => {
									console.log(err, "Coinmarketcap service could not executed...");
								});
							}
						}
					}).catch(err => {
						res.status(500).json({
							"error": err
						});
					});

					
				});

			}).catch(err => {
				console.log(err, "OfferwallList service could not executed...");
			});

		
		}
	});
};