module.exports = (sequelize, Sequelize) => {
	const References = sequelize.define('users2s', {
	  username: {
		  type: Sequelize.STRING
	  },
	  first_name: {
		  type: Sequelize.STRING
	  },
	  last_name: {
		  type: Sequelize.STRING
	  },
	  country: {
		  type: Sequelize.STRING
	  }
	});
	
	return References;
}