module.exports = (sequelize, Sequelize) => {
	const UserCoins = sequelize.define('user_coins', {
	  id: {
		  type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
	  },
	  user_id: {
		  type: Sequelize.INTEGER
	  },
	  coin_id: {
		  type: Sequelize.INTEGER
	  },
	  total: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return UserCoins;
}