module.exports = (sequelize, Sequelize) => {
	const Transactions = sequelize.define('transactions', {
	  user_id: {
		  type: Sequelize.STRING
	  },
	  amount: {
		  type: Sequelize.STRING
	  },
	  type: {
		  type: Sequelize.STRING
	  },
	  payment_status: {
		  type: Sequelize.STRING
	  },
	  status: {
		  type: Sequelize.STRING
	  },
	  created_date: {
		  type: Sequelize.STRING
	  },
	  confirm_date: {
		  type: Sequelize.STRING
	  }
	}, {
	    timestamps: false
	});
	
	return Transactions;
}