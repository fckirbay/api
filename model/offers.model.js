module.exports = (sequelize, Sequelize) => {
	const Offers = sequelize.define('offers', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  offer_id: {
		type: Sequelize.STRING
	  },
	  name: {
		  type: Sequelize.STRING
	  },
	  anchor: {
		  type: Sequelize.STRING
	  },
	  requirements: {
		  type: Sequelize.STRING
	  },
	  user_agent: {
		  type: Sequelize.STRING
	  },
	  mobile_only: {
		type: Sequelize.STRING
	  },
	  icon: {
		type: Sequelize.STRING
	  },
	  epc: {
		type: Sequelize.STRING
	  },
	  categories: {
		type: Sequelize.STRING
	  },
	  cpm: {
		type: Sequelize.INTEGER
	  },
	  description: {
		type: Sequelize.STRING
	  },
	  countries: {
		type: Sequelize.INTEGER
	  },
	  offer_type: {
		type: Sequelize.STRING
	  },
	  media: {
		type: Sequelize.STRING
	  },
	  payout: {
		type: Sequelize.STRING
	  },
	  click_url: {
		type: Sequelize.STRING
	  },
	  support_url: {
		type: Sequelize.STRING
	  },
	  preview_url: {
		type: Sequelize.STRING
	  },
	  status: {
		type: Sequelize.STRING
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Offers;
}