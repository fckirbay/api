module.exports = (sequelize, Sequelize) => {
	const Boxes = sequelize.define('boxes', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  status: {
		  type: Sequelize.STRING
	  },
	  coin_id: {
		  type: Sequelize.STRING
	  },
	  user_id: {
		  type: Sequelize.STRING
	  },
	  prize: {
		  type: Sequelize.STRING
	  },
	  created_at: {
		type: Sequelize.DATE
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Boxes;
}