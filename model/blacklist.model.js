module.exports = (sequelize, Sequelize) => {
	const Blacklist = sequelize.define('blacklist', {
	  firebase: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Blacklist;
}