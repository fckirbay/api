module.exports = (sequelize, Sequelize) => {
	const Bonuses = sequelize.define('bonuses', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  user_id: {
		type: Sequelize.STRING
	  },
	  bonus_amount: {
		  type: Sequelize.STRING
	  },
	  reason: {
		  type: Sequelize.STRING
	  },
	  date: {
		  type: Sequelize.DATE
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Bonuses;
};