module.exports = (sequelize, Sequelize) => {
	const Stats = sequelize.define('stats', {
	  total_tasks: {
		  type: Sequelize.STRING
	  },
	  completed: {
		  type: Sequelize.STRING
	  },
	  rewarded: {
		  type: Sequelize.STRING
	  }
	}, {
	    freezeTableName: true
	}, {
	    timestamps: false
	});
	
	return Stats;
}