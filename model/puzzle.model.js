module.exports = (sequelize, Sequelize) => {
	const Puzzle = sequelize.define('puzzle', {
	  id: {
		  type: Sequelize.STRING,
		  primaryKey: true
	  },
	  user_id: {
		  type: Sequelize.STRING
	  },
	  score: {
		  type: Sequelize.STRING
	  },
	  datetime: {
		  type: Sequelize.DATE
	  },
	  is_winner: {
		  type: Sequelize.STRING
	  },
	  completed_at: {
		  type: Sequelize.DATE
	  }
	}, {
	    timestamps: false,
	    freezeTableName: true
	});
	
	return Puzzle;
}